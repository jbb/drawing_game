use std::convert::TryInto;

// Serde
use serde_derive::*;

// Database
use diesel::Queryable;
use diesel::prelude::*;
use crate::schema::actions;
use crate::schema::groups;
use crate::schema::users;

use rand::Rng;

#[derive(Queryable, Insertable)]
pub struct Action {
    pub group_id: i32,
    pub user: i32,
    pub thread: i32,
    pub round: i32,
    pub text: Option<String>,
    pub drawing: Option<String>
}

#[derive(Queryable, Insertable, Serialize)]
pub struct User {
    pub user: i32,
    pub group_id: i32,
    pub nickname: String,
    pub picture: String
}

#[derive(Queryable, Insertable)]
pub struct Group {
    pub group_id: i32,
    pub rounds: i32
}

pub fn create_game(conn: &diesel::SqliteConnection, rounds: i32) -> i32 {
    let group = Group {
        group_id: rand::thread_rng().gen(),
        rounds
    };
    println!("Creating game");
    diesel::insert_into(groups::table)
        .values(&group)
        .execute(conn)
        .expect("Failed to create group");

    return group.group_id;
}

pub fn join_game(conn: &diesel::SqliteConnection, group: i32, nick: String) -> User {
    let user = User {
        group_id: group,
        nickname: nick,
        picture: String::new(),
        user: rand::thread_rng().gen()
    };

    diesel::insert_into(users::table)
        .values(&user)
        .execute(conn)
        .expect("Failed to create user");

    return user;
}

pub fn make_move(conn: &diesel::SqliteConnection, user: i32, group: i32, round: i32, thread: i32, text: Option<String>, picture: Option<String>) {
    let action = Action {
        group_id: group,
        round: round,
        text: text,
        drawing: picture,
        user: user,
        thread: thread
    };

    println!("Inserting action");
    diesel::insert_into(actions::table).values(&action).execute(conn).expect("Failed to store move");
}

pub fn game_started(conn: &diesel::SqliteConnection, group: i32) -> bool {
    let actions: Vec<Action> = actions::table
        .filter(actions::group_id.eq(&group))
        .load::<Action>(conn)
        .expect("Could not load the actions from the database");

    return actions.len() != 0
}

pub fn game_finished(conn: &diesel::SqliteConnection, group: i32) -> bool {
    let actions: Vec<Action> = actions::table
        .filter(actions::group_id.eq(&group))
        .load::<Action>(conn)
        .expect("Could not load the actions from the database");

    let users: Vec<User> = users::table
        .filter(users::group_id.eq(&group))
        .load::<User>(conn)
        .expect("Could not load the users from the database");

    let rounds: i32 = groups::table
        .filter(groups::group_id.eq(group))
        .limit(1)
        .load::<Group>(conn)
        .expect("Could not load group from the database")
        .get(0).expect("Group not found")
        .rounds;

    return (actions.len() / users.len()) > rounds.try_into().unwrap();
}

pub fn game_running(conn: &diesel::SqliteConnection, group: i32) -> bool {
    let started = game_started(conn, group);
    let finished = game_finished(conn, group);

    println!("started {}, finished {}", started, finished);

    started && !finished
}

pub fn all_ready(conn: &diesel::SqliteConnection, group: i32, round: i32) -> bool {
    let moves: usize = actions::table
        .filter(actions::group_id.eq(group))
        .filter(actions::round.eq(round))
        .load::<Action>(conn)
        .expect("Failed to fetch actions for current round")
        .len();

    let users: usize = users::table
        .filter(users::group_id.eq(&group))
        .load::<User>(conn)
        .expect("Could not load the users from the database")
        .len();

    moves == users
}

pub fn users_in_group(conn: &diesel::SqliteConnection, group: i32) -> Vec<User> {
    users::table.filter(users::group_id.eq(group))
        .load::<User>(conn)
        .expect("Failed to get list of users from database")
}