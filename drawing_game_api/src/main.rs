#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate diesel;

use rocket_contrib::json::Json;
use serde_derive::*;
use serde;

use diesel::prelude::*;

pub mod database;
pub mod schema;

#[database("game")]
struct GameDbConn(diesel::SqliteConnection);

#[derive(Serialize)]
struct GameResult {
    game_id: i32
}

#[get("/api/create_game?<rounds>")]
fn create_game(conn: GameDbConn, rounds: i32) -> Json<GameResult> {
    Json(
        GameResult {
            game_id: database::create_game(&*conn, rounds)
        }
    )
}

#[get("/api/join_game?<group>&<nick>")]
fn join_game(conn: GameDbConn, group: i32, nick: String) -> Json<database::User> {
    Json(
        database::join_game(&*conn, group, nick)
    )
}

/*
Plan:
    * Server prompts user that new round starts, sends thread id that user should fill in in this round, round id,
      and text / picture
    * user answers with make_move_text or make_move_picture
    * Server only accepts if there is no existing move with the thread idea for the round
*/

#[get("/api/make_move_text?<user>&<group>&<round>&<thread>&<text>")]
fn make_move_text(conn: GameDbConn, user: i32, group: i32, round: i32, thread: i32, text: String) {
    // TODO add checks
    database::make_move(&*conn, user, group, round, thread, Some(text), None)
}

#[get("/api/make_move_picture?<user>&<group>&<round>&<thread>&<picture>")]
fn make_move_picture(conn: GameDbConn, user: i32, group: i32, round: i32, thread: i32, picture: String) {
    // TODO add checks
    database::make_move(&*conn, user, group, round, thread, None, Some(picture))
}

#[derive(Serialize)]
struct GameRunningResult {
    game_running: bool
}

#[get("/api/game_running?<group>")]
fn game_running(conn: GameDbConn, group: i32) -> Json<GameRunningResult> {
    Json(
        GameRunningResult {
            game_running: database::game_running(&*conn, group)
        }
    )
}

#[derive(Serialize)]
struct AllReadyResult {
    all_ready: bool
}

#[get("/api/all_ready?<group>&<round>")]
fn all_ready(conn: GameDbConn, group: i32, round: i32) -> Json<AllReadyResult> {
    Json(
        AllReadyResult {
            all_ready: database::all_ready(&*conn, group, round)
        }
    )
}

#[get("/api/users_in_group?<group>")]
fn users_in_group(conn: GameDbConn, group: i32) -> Json<Vec<database::User>> {
    Json(
        database::users_in_group(&*conn, group)
    )
}

#[get("/")]
fn index(conn: GameDbConn) -> String {
    let id = database::create_game(&*conn, 3);
    let user = database::join_game(&*conn, id, "User one".to_string());
    database::join_game(&*conn, id, "User two".to_string());
    database::make_move(&*conn, user.user, id, 1, user.user, Some("Baum".to_owned()), None);
    let running = database::game_running(&*conn, id);

    format!("Game running: {}, all ready: {}", running, database::all_ready(&*conn, id, 1))
}

fn main() {
    rocket::ignite()
    .mount("/", routes![index, create_game, join_game, make_move_text, make_move_picture, game_running, all_ready, users_in_group])
    .attach(GameDbConn::fairing())
    .launch();
}