table! {
    actions (group_id) {
        group_id -> Integer,
        user -> Integer,
        thread -> Integer,
        round -> Integer,
        text -> Nullable<Text>,
        drawing -> Nullable<Text>,
    }
}

table! {
    groups (group_id) {
        group_id -> Integer,
        rounds -> Integer,
    }
}

table! {
    users (user) {
        user -> Integer,
        group_id -> Integer,
        nickname -> Text,
        picture -> Text,
    }
}

allow_tables_to_appear_in_same_query!(
    actions,
    groups,
    users,
);
