-- Your SQL goes here
CREATE TABLE groups (
    group_id INTEGER PRIMARY KEY NOT NULL,
    rounds INTEGER NOT NULL
);

CREATE TABLE actions (
    group_id INTEGER PRIMARY KEY NOT NULL,
    user INTEGER NOT NULL,
    thread INTEGER NOT NULL,
    round INTEGER NOT NULL,
    text TEXT,
    drawing VARCHAR
);

CREATE TABLE users (
    user INTEGER PRIMARY KEY NOT NULL,
    group_id INTEGER NOT NULL,
    nickname TEXT NOT NULL,
    picture VARCHAR NOT NULL
);